#!/bin/bash

# Copyright 2017 Mohamad Issawi
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

function dlchapter ()
{
	/usr/share/buka-parse --get-pages $1 |\
	parallel --bar --will-cite $PARALLELOPTS --gnu "wget -qnc {}"
}

# $1 is the format, $2 is the file name
function format ()
{
    if [ "$1" == "pdf" ]
    then
	convert * "../${2}.pdf"
    elif  [ "$1" == "cbz" ]
    then
	zip "../${2}.cbz" *
    fi
}

function help ()
{
    echo "usage: buka [OPTIONS] [URL]"
    echo ""
    echo "where URL is the url of the manga's chapter listing page."
}

while getopts "af:hj:" OPT
do
    case $OPT in
	a ) ALL=true;;
	f ) if [ "$OPTARG" == "pdf" ]
 	    then
		if hash convert 2>/dev/null
		then
		    FORMAT=$OPTARG
		else
		    echo "cannot create pdf, perhaps imagemagick is not installed?"
		    exit 1
		fi
	    elif [ "$OPTARG" == "cbz" ]
	    then
		if hash zip 2>/dev/null
		then
		    FORMAT="$OPTARG"
		else
		    echo "cannot create cbz, perhaps zip is not installed?"
		    exit 1
		fi
	    else
		echo "unknown format: $OPTARG"
		exit 1
	    fi;;
	h ) help
	    exit 0;;
	j ) PARALLELOPTS="-j${OPTARG}";;
	* ) exit 1;;
    esac
done
shift "$((OPTIND - 1))"
      
if [ $# -eq 0 ] 
then
    help
    exit 0
fi

WORKING_DIR=$(pwd)
URL=$1

# if ALL, then skip formatting for dialog
if [ "$ALL" = "true" ]
then
    LINKS=$(/usr/share/buka-parse --get-chapters $URL)
else
    CHAPTERS=$(/usr/share/buka-parse --get-dialog-chapters $URL)
    LINKS=$(dialog --stdout --no-tags --checklist "Select the chapters you would like to download:" 100 100 100 $CHAPTERS)
fi

if [ $? -eq 0 ] 
then
clear
read -a LINK_ARRAY <<< $LINKS
    for LINK in "${LINK_ARRAY[@]}"
    do
	TITLE=$(/usr/share/buka-parse --get-title $LINK)    
	mkdir -p "$TITLE"
	cd "$TITLE"
	echo "Downloading "$TITLE
	dlchapter $LINK
	# check if the images need formatting
	if [ -n "$FORMAT" ]
	then
	    format "$FORMAT" "$TITLE"
	    cd $WORKING_DIR
	    rm -r "$TITLE"
	else
	    cd $WORKING_DIR
	fi
    done    
fi
